# Python pour Blender, Damien Picard 2022


# @@ Sortie texte @@
print("Salut !")

# @@ Commentaires @@
# Un croisillon (#) en début de ligne
print(1)  # Ou bien en milieu de ligne

# @@ Déclarations @@
mon_booleen = True  # ou False (majuscule)
mon_entier = 1
mon_flottant = 4.20
ma_chaine = "Salut !"

# Python est dit à typage dynamique, les
# variables peuvent accueillir tous les types !
ma_variable = 1
# Et changer en cours de route
ma_variable = "Salut !"

# @@ Listes @@
# Les listes peuvent contenir
# différents types. On dit
# qu’elles ne sont pas typées
ma_liste = ["one", True, 3]
ma_liste.append("four")

# @@ Dictionnaires @@
mon_dict = {}
mon_dict["two"] = 2
mon_dict[1] = "one"

# @@ Boucles @@
# Les blocs sont signifiés par une
# indentation en début de ligne
for i in range(10):
    print(i)

# @@ Fonctions @@
def mon_addition(nombre1, nombre2):
    return nombre1 + nombre2

# @@ Conditions @@
if mon_entier > 8000:
    print("Sa puissance est à plus de 8000 !")

# @@ END @@
