// C# pour Unity, Damien Picard 2022


// @@ Sortie texte @@
Debug.Log("Salut !");

// @@ Commentaires @@
// Deux slashes en début de ligne
Debug.Log(1); // Ou bien en milieu de ligne

/* Ou bien un slash et une astérisque pour
   ouvrir un long commentaire, et une
   astérisque et un slash pour le fermer : */

// @@ Déclarations @@
bool mon_booleen = true; // ou false (minuscule)
int mon_entier = 1;
float mon_flottant = 4.20f;
string ma_chaine = "Salut !";

// C# est dit à "typage fort", c’est-à-dire
// qu’on déclare toujours le type et celui-ci
// ne peut pas changer !
int ma_variable = 1;
ma_variable = "Salut !"; // Erreur de compilation

// @@ Listes @@
// Il faut inclure l’en-tête :
using Collection.Generic;

List<string> ma_liste = new List<string>("one", "two", "three");
ma_liste.Add("four");

// @@ Dictionnaires @@
// Il faut inclure l’en-tête :
using Collection.Generic;

Dictionary<string, int> mon_dict = new Dictionary<string, int>();
mon_dict["one"] = 1;
mon_dict["two"] = 2;

// @@ Boucles @@
// Les blocs sont signifiés par des accolades { }
for (int i = 0; i < 10; i++)
    {
    Debug.Log(i);
    }

// @@ Fonctions @@
int mon_addition(int nombre1, int nombre2)
    {
    return nombre1 + nombre2;
    }

// @@ Conditions @@
if (mon_entier > 8000)
    {
    Debug.Log("Sa puissance est à plus de 8000 !");
    }

// @@ END @@
